﻿using System;
using System.Collections;
using System.Collections.Generic;
using CastleBuilder.IsometricMaths;
using CastleBuilder.Map;
using CastleBuilder.WorkShop;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Tilemaps;
using Util;

namespace CastleBuilder.Grid
{
    [System.Serializable]
    public class GridData
    {
        public float size;
        public float unit;
    }
    public class GridSystem : MonoBehaviour
    {
        public static GridSystem pInstance { get; private set; }
        public GridData pGridData { get; private set; }
        
        private float pSize
        {
            get { return pGridData == null ? 1.25f : pGridData.size; }
        }
        
        private float pUnit
        {
            get { return pGridData == null ? 40f : pGridData.unit; }
        }

#if UNITY_EDITOR
        private string path => Application.dataPath + "/Grid.xml";
#else
        private string path => Application.persistentDataPath + "/Grid.xml";
#endif
        [SerializeField] private Mesh m_Quad;
        [SerializeField] private Material m_TileOddMat;
        [SerializeField] private Material m_TileEvenMat;
        
        private void Awake()
        {
            pInstance = this;
            LoadData();
        }

        private void Start()
        {
            var offset = pSize * pUnit * -0.5f;
            transform.position = new Vector3(offset, offset, 0f);
            UpdateGrid();
            ShowGrid(false);
        }

        private void UpdateGrid()
        {
            var gridPoints=new List<Vector3>();
            for (int i = 0; i < pUnit; i++)
            {
                for (int j = 0; j < pUnit; j++)
                {
                    var centerpoint=new Vector3(i*pSize,j*pSize,0f) + transform.position;
                    SpawnWorkShopItem(centerpoint.ToIsometric(), i,j,pSize);
                }
            }
        }
        
        public void SpawnWorkShopItem(Vector3 position,int x, int y,float size)
        {
            EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var archetype = entityManager.CreateArchetype(
                typeof(GridDataComponent),
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(RenderBounds),
                typeof(WorldRenderBounds),
                typeof(ChunkWorldRenderBounds),
                typeof(Scale));
            NativeArray<Entity> nativeArray = new NativeArray<Entity>(1, Allocator.Temp);
            entityManager.CreateEntity(archetype, nativeArray);
            var mat = (x + y) % 2 == 0 ? m_TileEvenMat : m_TileOddMat;
            foreach (var entity in nativeArray)
            {
                entityManager.SetSharedComponentData(entity,new RenderMesh()
                {
                    mesh = m_Quad,
                    material = mat,
                });
                entityManager.SetComponentData(entity, new GridDataComponent()
                {
                    coordinate = new int2(x,y)
                });
                entityManager.SetComponentData(entity,new Scale()
                {
                    Value = size*100f
                });
                entityManager.SetComponentData(entity,new Translation()
                {
                    Value = new float3(position.x,position.y,1f)
                });
            }
            nativeArray.Dispose();

        }

        public void ShowGrid(bool value)
        {
            m_TileEvenMat.color = value ? new Color(0f, 0f, 1f, 0.3f) : new Color(0f, 0f, 1f, 0f);
            m_TileOddMat.color = value ? new Color(1f, 0f, 0f, 0.3f) : new Color(1f, 0f, 0f, 0f);
        }

        private void LoadData()
        {
            var fileExit = false;
            var json = path.ReadFile(out fileExit);
            if (string.IsNullOrEmpty(json))
            {
                pGridData = new GridData()
                {
                    size = 1.25f,
                    unit = 40f
                };
                SaveGridData();
            }
            else
            {
                try
                {
                    pGridData = JsonUtility.FromJson<GridData>(json);
                }
                catch (Exception ex)
                {
                    Debug.LogError($"fail to parse data as modified or corrupted");
                    pGridData = new GridData()
                    {
                        size = 1.25f,
                        unit = 40f
                    };
                    path.DeleteFile();
                    SaveGridData();
                }
            }
        }

        private void SaveGridData()
        {
            path.WriteFile(JsonUtility.ToJson(pGridData, true));
        }

#if UNITY_EDITOR
    
        private void OnDrawGizmos()
        {
            for (int i = 0; i < pUnit; i++)
            {
                for (int j = 0; j < pUnit; j++)
                {
                    var centerpoint=new Vector2(i*pSize,j*pSize) + (Vector2)transform.position;
                    DrawPolygon(centerpoint, pSize, pSize);
                    GUIStyle guiStyle= new GUIStyle();
                    guiStyle.alignment = TextAnchor.MiddleCenter;
                    guiStyle.normal.textColor=Color.blue;
                    UnityEditor.Handles.Label(centerpoint.ToIsometric(),$"({i},{j})",guiStyle);
                }
            }
        }

        private void DrawPolygon(Vector2 center,float w,float h)
        {
            var bl = center + Vector2.down * h * 0.5f + Vector2.left * w * 0.5f;
            var br = center + Vector2.down * h * 0.5f + Vector2.right * w * 0.5f;
            var tl = center + Vector2.up * h * 0.5f + Vector2.left * w * 0.5f;
            var tr = center + Vector2.up * h * 0.5f + Vector2.right * w * 0.5f;
            Gizmos.color=Color.white;
            Gizmos.DrawLine(bl.ToIsometric(),br.ToIsometric());
            Gizmos.DrawLine(br.ToIsometric(),tr.ToIsometric());
            Gizmos.DrawLine(tr.ToIsometric(),tl.ToIsometric());
            Gizmos.DrawLine(tl.ToIsometric(),bl.ToIsometric());
            //Original
            // Gizmos.color=Color.yellow;
            // Gizmos.DrawLine(bl,br);
            // Gizmos.DrawLine(br,tr);
            // Gizmos.DrawLine(tr,tl);
            // Gizmos.DrawLine(tl,bl);
        }
#endif
        
    }
    
    
}
