﻿using System.ComponentModel;
using Unity.Mathematics;
using Unity.Entities;


namespace CastleBuilder.Grid
{
    public struct GridDataComponent : IComponentData
    {
        public int2 coordinate;
    }
}
