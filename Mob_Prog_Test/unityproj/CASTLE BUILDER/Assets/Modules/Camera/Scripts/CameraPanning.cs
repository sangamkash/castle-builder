﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CastleBuilder.ECS.Placement;
using CastleBuilder.Grid;
using CastleBuilder.IsometricMaths;
using CastleBuilder.Map;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.EventSystems;

namespace CastleBuilder.CameraSystem
{
    /// <summary>
    /// This script control the panning and zooming of camera 
    /// </summary>
    public class CameraPanning : MonoBehaviour
    {
        public Camera pCamera
        {
            get
            {
                if(mCamera == null)
                    mCamera=Camera.main;
                return mCamera;
            }
        }
        private bool pIsItemUnplaced => PlacementSystem.pIsItemUnplaced;
        private MapSystem pMapSystem=>MapSystem.pInstance;
        private float pGridSize=>GridSystem.pInstance.pGridData.size;
        [SerializeField] private Transform m_ThisTansform;
        [SerializeField] private Vector2 m_PanningAreaInner;
        [SerializeField] private Vector2 m_PanningAreaOuter;
        [SerializeField] private AnimationCurve eslaticAnim;
        [SerializeField] private float eslaticTime = 0.5f;
        [SerializeField] private float m_MaxZoom = 2f;
        [SerializeField] private float m_MinZoom = 7f;
        [SerializeField] private float m_ZoomInSpeed = 2f;
        private float eslaticstartTime;
        private Camera mCamera;
        private bool mTouchStarted;
        private Vector2 mlastTouchPosition;
        private float mZoomState;
        [SerializeField]private int touchId=-1;
        private void Update()
        {
#if UNITY_EDITOR
            
            MouseControl();
#else
            TouchControl();
#endif
        }

        private void MouseControl()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var touchPos=Input.mousePosition;
                var worldPoint = ((Vector2) pCamera.ScreenToWorldPoint(touchPos));
                var isTileOccupied = pMapSystem.IsTileOccupied(worldPoint.ToNearestIsometricPoint(pGridSize));
                var isPointerOverUI = EventSystem.current.IsPointerOverUIObject();
                mTouchStarted = true && !isTileOccupied && !isPointerOverUI;
                mlastTouchPosition = Input.mousePosition;
            }
            else if(Input.GetMouseButtonUp(0))
            {
                mTouchStarted = false;
            }

            if (mTouchStarted && !pIsItemUnplaced)
            {
                var pos = m_ThisTansform.position;
                var deta = ((Vector2)Input.mousePosition - mlastTouchPosition);
                var deltaPos = Vector3.ProjectOnPlane(deta, -Vector3.forward);
                var camConst = (1f/Screen.height * pCamera.orthographicSize) / 0.5f;
                pos += -deltaPos * camConst;
                mlastTouchPosition = Input.mousePosition;
                m_ThisTansform.position = GetContactPoint(m_PanningAreaOuter, pos);
                eslaticstartTime = Time.time;
            }
            else
            {
                Rect rect= new Rect(Vector2.zero,m_PanningAreaInner*0.5f);
                if (!rect.Contains(m_ThisTansform.position))
                {
                    var lerpValue = eslaticAnim.Evaluate((Time.time - eslaticstartTime) / eslaticTime);
                    var targetPos = GetContactPoint(m_PanningAreaInner, m_ThisTansform.position);
                    m_ThisTansform.position=  Vector3.Lerp(m_ThisTansform.position,targetPos,lerpValue);
                }
            }

            if (mZoomState != 0)
            {
                var size = pCamera.orthographicSize;
                size -= Time.deltaTime * m_ZoomInSpeed*mZoomState;
                pCamera.orthographicSize = Mathf.Clamp(size, m_MinZoom, m_MaxZoom);
            }
        }

        private void TouchControl()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if(touchId == -1)
                    touch = Input.GetTouch(0);
                {
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        var temp=Input.GetTouch(i);
                        if (temp.fingerId == touchId)
                            touch = temp;
                    } 
                }
                if (touch.phase == TouchPhase.Began && touchId == -1)
                {
                    touchId = touch.fingerId;
                    var touchPos = touch.position;
                    var worldPoint = ((Vector2) pCamera.ScreenToWorldPoint(touchPos));
                    var isTileOccupied = pMapSystem.IsTileOccupied(worldPoint.ToNearestIsometricPoint(pGridSize));
                    var isPointerOverUI = EventSystem.current.IsPointerOverUIObject();
                    mTouchStarted = true && !isTileOccupied && !isPointerOverUI;
                    mlastTouchPosition = touch.position;
                }
                else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
                    touchId = -1;
                    mTouchStarted = false;
                }

                if (mTouchStarted && !pIsItemUnplaced)
                {
                    var pos = m_ThisTansform.position;
                    var deta = (touch.position - mlastTouchPosition);
                    var deltaPos = Vector3.ProjectOnPlane(deta, -Vector3.forward);
                    var camConst = (1f / Screen.height * pCamera.orthographicSize) / 0.5f;
                    pos += -deltaPos * camConst;
                    mlastTouchPosition = touch.position;
                    m_ThisTansform.position = GetContactPoint(m_PanningAreaOuter, pos);
                    eslaticstartTime = Time.time;
                }
                else
                {
                    Rect rect = new Rect(Vector2.zero, m_PanningAreaInner * 0.5f);
                    if (!rect.Contains(m_ThisTansform.position))
                    {
                        var lerpValue = eslaticAnim.Evaluate((Time.time - eslaticstartTime) / eslaticTime);
                        var targetPos = GetContactPoint(m_PanningAreaInner, m_ThisTansform.position);
                        m_ThisTansform.position = Vector3.Lerp(m_ThisTansform.position, targetPos, lerpValue);
                    }
                }
                if (mZoomState != 0)
                {
                    var size = pCamera.orthographicSize;
                    size -= Time.deltaTime * m_ZoomInSpeed * mZoomState;
                    pCamera.orthographicSize = Mathf.Clamp(size, m_MinZoom, m_MaxZoom);
                }
            }
        }

        public void ZoomIn()
        {
            mZoomState = 1f;
        }

        public void ZoomOut()
        {
            mZoomState = -1f;
        }

        public void ZoomNeutral()
        {
            mZoomState = 0f;
        }
        private Vector3 GetContactPoint(Vector2 area,Vector3 point)
        {
            return new Vector3(Mathf.Clamp(point.x, -area.x * 0.5f, area.x * 0.5f),
                Mathf.Clamp(point.y, -area.y * 0.5f, area.y * 0.5f), point.z);
        }
        private void OnDrawGizmos()
        {
            Gizmos.color=new Color(0f,1f,0,0.5f);
            Gizmos.DrawWireCube(Vector3.zero, new Vector3(m_PanningAreaInner.x,m_PanningAreaInner.y,2f));
            Gizmos.color=new Color(0f,1f,1f,0.5f);
            Gizmos.DrawWireCube(Vector3.zero, new Vector3(m_PanningAreaOuter.x,m_PanningAreaOuter.y,2f));
        }
    }
}
