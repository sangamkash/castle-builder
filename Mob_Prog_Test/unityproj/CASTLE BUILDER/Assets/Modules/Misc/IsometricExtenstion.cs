﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CastleBuilder.IsometricMaths
{
    public static class IsometricExtenstion 
    {
        /// <summary>
        /// convert cartesian coordinate to Isometric coordinate (2D)
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public static Vector2 ToIsometric(this Vector2 coordinate)
        {
            var isoX = coordinate.x + coordinate.y;
            var isoY = (coordinate.y - coordinate.x) / 2f;
            return new Vector2(isoX, isoY);
        }
        
        /// <summary>
        /// convert Isometric coordinate to cartesian coordinate (2D)
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public static Vector2 ToCartesian(this Vector2 coordinate)
        {
            var carX = coordinate.x/2f - coordinate.y;
            var carY = coordinate.y + coordinate.x / 2f;
            return new Vector2(carX, carY);
        }
        
        /// <summary>
        /// convert cartesian coordinate to Isometric coordinate
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public static Vector3 ToIsometric(this Vector3 coordinate)
        {
            var isoX = coordinate.x + coordinate.y;
            var isoY = (coordinate.y - coordinate.x) / 2f;
            return new Vector3(isoX, isoY,0f);
        }
        
        /// <summary>
        /// convert Isometric coordinate to cartesian coordinate
        /// </summary>
        /// <param name="coordinate"></param>
        /// <returns></returns>
        public static Vector3 ToCartesian(this Vector3 coordinate)
        {
            var carX = coordinate.x/2f - coordinate.y;
            var carY = coordinate.y + coordinate.x / 2f;
            return new Vector3(carX, carY, 0f);
        }
        
        /// <summary>
        /// Convert Isometric coordinate to nearest Tile point Isometric coordinate with given grid size (2d)
        /// </summary>
        /// <param name="coordinate"> Isometric coordinate</param>
        /// <param name="gridSize"></param>
        /// <returns></returns>
        public static Vector2 ToNearestIsometricPoint(this Vector2 coordinate,float gridSize)
        {
            var car = coordinate.ToCartesian();
            return  car.RoundTo(gridSize).ToIsometric();
        }

        /// <summary>
        /// Convert Isometric coordinate to nearest Tile point Isometric coordinate with given grid size (2d)
        /// </summary>
        /// <param name="coordinate"> Isometric coordinate</param>
        /// <param name="gridSize"></param>
        /// <returns></returns>
        public static Vector3 ToNearestIsometricPoint(this Vector3 coordinate,float gridSize)
        {
            var car = coordinate.ToCartesian();
            return  car.RoundTo(gridSize).ToIsometric();
        }

        private static Vector3 RoundTo(this ref Vector3 point, float unit)
        {
            return new Vector3(point.x.RoundTo(unit), point.y.RoundTo(unit), 0f);
        }
        
        private static Vector2 RoundTo(this ref Vector2 point, float unit)
        {
            return new Vector2(point.x.RoundTo(unit), point.y.RoundTo(unit));
        }
        private static float RoundTo(this float point, float unit)
        {
            var r = point % unit;
            var value = point - r;
            if (r >= 0)
                return r > unit * 0.5f ? value + unit : value;
            else
                return r < -unit * 0.5f ? value - unit : value;
        }
        
        /// <summary>
        /// Inform if pointer is on UI
        /// </summary>
        /// <param name="eventSystem"></param>
        /// <returns></returns>
        public static bool IsPointerOverUIObject(this EventSystem eventSystem)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(eventSystem);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
        
        /// <summary>
        /// Inform in touch in on UI
        /// </summary>
        /// <param name="eventSystem"></param>
        /// <param name="touch"></param>
        /// <returns></returns>
        public static bool IsFingureOverUIObject(this EventSystem eventSystem,Touch touch)
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(eventSystem);
            eventDataCurrentPosition.position = new Vector2(touch.position.x, touch.position.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}