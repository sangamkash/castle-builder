﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Util
{
    public static class FileExtensions
    {
        public static string ReadFile(this string path,out bool exit)
        {
            exit = File.Exists(path);
            if (exit)
            {
                return File.ReadAllText(path);
            }
            return string.Empty;
        }

        public static void WriteFile(this string path, string data)
        {
            //TODO find better way to write file Temp fix as file writing is already created file is creating issue 
            path.DeleteFile();
            var file = File.Open(path, FileMode.OpenOrCreate);
            var bytes = Encoding.UTF8.GetBytes(data);
            file.Write(bytes, 0, bytes.Length);
            file.Close();
        }

        public static bool DeleteFile(this string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                return true;
            }
            return false;
        }

        public static bool FileExit(this string path)
        {
            return File.Exists(path);
        }
    }
}
