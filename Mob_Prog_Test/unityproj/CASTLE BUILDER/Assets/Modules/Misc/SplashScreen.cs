﻿using System;
using System.Collections;
using System.Collections.Generic;
using CastleBuilder;
using UnityEngine;
using UnityEngine.SceneManagement;
using Util;

public class SplashScreen : MonoBehaviour
{
    private void Start()
    {
        Invoke("LoadSceneMode",2f);
    }

    private void LoadSceneMode()
    {
        if (PlayerDataManager.pInstance.PlayerDataExist())
            SceneManager.LoadScene(GameConstants.SceneNameConstants.GamePlay);
        else
            SceneManager.LoadScene(GameConstants.SceneNameConstants.Startup);
        
    }
}
