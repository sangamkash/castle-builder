﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
    public static class GameConstants
    {
        public static class SceneNameConstants
        {
            public const string Splash = "Splash";
            public const string Startup = "StartUp";
            public const string GamePlay = "GamePlay";
        } 
    }
}
