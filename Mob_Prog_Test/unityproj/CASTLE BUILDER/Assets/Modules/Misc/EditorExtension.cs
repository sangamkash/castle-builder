﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Util
{

#if UNITY_EDITOR
    public static class EditorExtension
    {
        public const string RESORUCES = "Resources";

        public static string GetPath(this Object obj)
        {
            return AssetDatabase.GetAssetPath(obj);
        }

        public static string GetResourcePath(this string path)
        {

            if (!IsValidResourcePath(path))
            {
                Debug.LogError("Invalid Resource Path");
                return string.Empty;
            }

            Debug.LogError($"{path}");
            var strs = path.Split('/');
            var tempPath = "";
            var state = false;
            foreach (var str in strs)
            {
                if (str == RESORUCES)
                {
                    state = true;
                }
                else if (state)
                {
                    tempPath += str + "/";
                }
            }

            var resourcePath = tempPath.Split('.');
            Debug.LogError($"{tempPath}");
            Debug.LogError($"{resourcePath[0]}  {resourcePath[1]}");
            return resourcePath[0];
        }

        public static bool IsValidResourcePath(string path)
        {
            if (path.Contains(RESORUCES))
            {
                if (!path.Replace(RESORUCES, String.Empty).Contains(RESORUCES))
                    return true;
            }

            return false;
        }
    }
#endif
}
