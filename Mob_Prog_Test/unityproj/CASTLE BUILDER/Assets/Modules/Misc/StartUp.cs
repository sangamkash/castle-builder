﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Util;
using TMPro;

namespace CastleBuilder.Begin
{
    /// <summary>
    /// This is a start up screen used for loading the main game 
    /// </summary>
    public class StartUp : MonoBehaviour
    {
        [SerializeField] private TMP_InputField mNameInput;

        [SerializeField] private GameObject mWarningGameObj;
        
        public void LoadGame()
        {
            var name = mNameInput.text;
            if (name.Length >= 3)
            {
                PlayerDataManager.pInstance.GetPlayerData().playerName = name;
                PlayerDataManager.pInstance.SavePlayerData();
                SceneManager.LoadScene(GameConstants.SceneNameConstants.GamePlay);
            }
            else
            {
                CancelInvoke();
                mWarningGameObj.SetActive(true);
                Invoke("DisableWarning", 1.5f);
            }
        }

        private void DisableWarning()
        {
            mWarningGameObj.SetActive(false);
        }
    }
}
