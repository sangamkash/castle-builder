﻿using System;
using System.Collections;
using System.Collections.Generic;
using CastleBuilder.Grid;
using UnityEngine;
using Util;

namespace CastleBuilder
{
    [System.Serializable]
    public class PlayerData
    {
        public string playerName;
        public uint coin;
        public uint happiness;
        public uint level;
    }
    
    /// <summary>
    /// This script take care of all Player data related things such saveing ,updating and creating data
    /// </summary>
    public class PlayerDataManager
    {

        private static PlayerDataManager _pInstance;
        public static PlayerDataManager pInstance
        {
            get
            {
                if (_pInstance == null)
                {
                    _pInstance = new PlayerDataManager();
                }
                return _pInstance;
            }
        }
#if UNITY_EDITOR
        private string mPath = Application.dataPath + "/PlayerData.txt";
#else
        private string mPath = Application.persistentDataPath + "/PlayerData.txt";
#endif
        private static PlayerData mPlayerData;
        public event Action<PlayerData> OnPlayerDataChange;
        public PlayerData GetPlayerData()
        {
            if (mPlayerData == null)
            {
                var fileExit = false;
                var json = mPath.ReadFile(out fileExit);
                if (string.IsNullOrEmpty(json))
                {
                    mPlayerData = DefaultData();
                    SavePlayerData();
                }
                else
                {
                    try
                    {
                        mPlayerData = JsonUtility.FromJson<PlayerData>(json);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError($"fail to parse data as modified or corrupted \n :: {json}");
                        mPlayerData = DefaultData();
                        mPath.DeleteFile();
                        SavePlayerData();
                    }
                }
            }
            return mPlayerData;
        }

        public  bool PlayerDataExist()
        {
            return mPath.FileExit();
        }
        
        public  void SavePlayerData()
        {
            mPath.WriteFile(JsonUtility.ToJson(mPlayerData, true));
            OnPlayerDataChange?.Invoke(mPlayerData);
        }

        public bool Purchase(uint amount)
        {
            var totalCoin = GetPlayerData().coin;
            var coinRemaining = totalCoin - amount;
            if (coinRemaining >= 0)
            {
                GetPlayerData().coin = coinRemaining;
                SavePlayerData();
                return true;
            }
            return false;
        }

        private PlayerData DefaultData()
        {
            return new PlayerData()
            {
                playerName = "DefaultName",
                coin = 100000,
                happiness = 100,
                level = 10
            };
        }
        
    }
}
