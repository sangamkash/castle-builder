﻿using System.Collections;
using System.Collections.Generic;
using CastleBuilder.Decoration.Data;
using UnityEngine;
using Util;

namespace CastleBuilder.Decoration
{
    #region  Data

    [System.Serializable]
    public class DecorationConfigData
    {
#if UNITY_EDITOR
        public Sprite decorationImg;
#endif
        public DecorationData decorationData;

#if UNITY_EDITOR
        public void AssignResourcePath()
        {
            decorationData.imagePath = decorationImg.GetPath().GetResourcePath();
        }
#endif
    }
    #endregion
    [CreateAssetMenu(fileName = "DecorationData", menuName = "CastleBuilder/Decoration", order = 2)]
    public class DecorationConfig : ScriptableObject
    {
        public DecorationConfigData[] decorationConfigDatas;
#if UNITY_EDITOR
        [ContextMenu("AssignPath")]
        public void UpdateResourcePath()
        {
            foreach (var item in decorationConfigDatas)
            {
                item.AssignResourcePath();
            }
        }
#endif
    }
}
