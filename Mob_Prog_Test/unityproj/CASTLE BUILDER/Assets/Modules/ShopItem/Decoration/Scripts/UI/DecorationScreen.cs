﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CastleBuilder.Decoration.UI
{

    public class DecorationScreen : MonoBehaviour
    {
        [Header("External Dependency")] 
        [SerializeField] private GameObject m_DecorationItemPrefab;
        [SerializeField] private DecorationConfig m_DecorationConfig;
        [Header("Internal Dependency")] 
        [SerializeField] private RectTransform m_Container;
        [SerializeField] private GameObject m_ShopScreen;

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            for (var i = 0; i < m_DecorationConfig.decorationConfigDatas.Length; i++)
            {
                var data = m_DecorationConfig.decorationConfigDatas[i];
                var item = Instantiate(m_DecorationItemPrefab, m_Container).GetComponent<DecorationItem>();
                item.Init(data,OnDecorationSelected);
            }
        }

        private void OnDecorationSelected(DecorationConfigData data)
        {
            m_ShopScreen.SetActive(false);
            ShopItemSpawner.pInstance.SpawnWorkShopItem(data.decorationData.imagePath, data.decorationData.id);
        }
    }
}
