﻿using System;
using System.Collections;
using System.Collections.Generic;
using CastleBuilder.Decoration;
using CastleBuilder.WorkShop;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CastleBuilder.Decoration.UI
{
    public class DecorationItem : ShopItem
    {
        private Action<DecorationConfigData> mAction;
        private DecorationConfigData mData;
        public void Init(DecorationConfigData data, Action<DecorationConfigData> action)
        {
            mAction = action;
            mData = data;
            m_Image.sprite = Resources.Load<Sprite>(data.decorationData.imagePath);
            m_Tile.text = data.decorationData.name;
            m_Cost.text = data.decorationData.goldCost.ToString();
        }
        public override void OnPointerDown()
        {
            if (PlayerDataManager.pInstance.Purchase(mData.decorationData.goldCost))
                base.OnPointerDown();
            else
                UIManager.pInstance.ShowWaring("Don't have enough balance");
        }
        public override void OnSelectionCompleted()
        {
            mAction?.Invoke(mData);
        }
    }
}
