﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using CastleBuilder.ECS.Placement;
using CastleBuilder.WorkShop;
using CastleBuilder.WorkShop.Data;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace CastleBuilder
{
    public class ShopItemSpawner : MonoBehaviour
    {
        private bool pIsItemUnplaced => PlacementSystem.pIsItemUnplaced;
        private UIManager pUIManager => UIManager.pInstance;
        [SerializeField] private Mesh m_Quad;
        [SerializeField] private Shader m_Shader;
        private Dictionary<string, Material> mMatDic;
        public static ShopItemSpawner pInstance { get; private set; }
        private void Awake()
        {
            pInstance = this;
        }

        
        public void SpawnWorkShopItem(string texturePath,int id)
        {
            if (pIsItemUnplaced)
            {
                pUIManager.ShowWaring("<color=red>Fixed the unplaced item first </color>");
                return;
            }
            EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var archetype = entityManager.CreateArchetype(
                typeof(WorkShopComponent),
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(RenderBounds),
                typeof(WorldRenderBounds),
                typeof(ChunkWorldRenderBounds),
                typeof(Scale));
            NativeArray<Entity> nativeArray = new NativeArray<Entity>(1, Allocator.Temp);
            entityManager.CreateEntity(archetype, nativeArray);
            foreach (var entity in nativeArray)
            {
                entityManager.SetSharedComponentData(entity,new RenderMesh()
                {
                    mesh = m_Quad,
                    material = GetMaterial(texturePath)
                });
                entityManager.SetComponentData(entity, new WorkShopComponent()
                {
                    Id=id,
                    placed = false
                });
                entityManager.SetComponentData(entity,new Scale()
                {
                    Value = 1.5f
                });
            }

            nativeArray.Dispose();

        }
        
        public void SpawnWorkShopItem(string texturePath,int id,Vector3 position)
        {
            EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var archetype = entityManager.CreateArchetype(
                typeof(WorkShopComponent),
                typeof(Translation),
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(RenderBounds),
                typeof(WorldRenderBounds),
                typeof(ChunkWorldRenderBounds),
                typeof(Scale));
            NativeArray<Entity> nativeArray = new NativeArray<Entity>(1, Allocator.Temp);
            entityManager.CreateEntity(archetype, nativeArray);
            foreach (var entity in nativeArray)
            {
                entityManager.SetSharedComponentData(entity, new RenderMesh()
                {
                    mesh = m_Quad,
                    material = GetMaterial(texturePath)
                });
                entityManager.SetComponentData(entity, new WorkShopComponent()
                {
                    Id = id,
                    placed = true
                });
                entityManager.SetComponentData(entity, new Translation()
                {
                    Value = new float3(position.x, position.y, position.z)
                });
                entityManager.SetComponentData(entity, new Scale()
                {
                    Value = 1.5f
                });
            }

            nativeArray.Dispose();

        }

        private Material GetMaterial(string path)
        {
            if(mMatDic== null)
                mMatDic= new Dictionary<string, Material>();
            if (mMatDic.ContainsKey(path))
                return mMatDic[path];
            var texture = Resources.Load<Texture>(path);
            var mat= new Material(m_Shader)
            {
                mainTexture = texture,
                color=Color.white
            };
            mMatDic.Add(path,mat);
            return mat;
        }
    }
}
