﻿using System;
using System.Collections;
using System.Collections.Generic;
using CastleBuilder.WorkShop;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CastleBuilder
{
    public abstract class ShopItem : MonoBehaviour
    {
        [SerializeField] protected Image m_Image;
        [SerializeField] protected Image m_FillImage;
        [SerializeField] protected TextMeshProUGUI m_Tile;
        [SerializeField] protected TextMeshProUGUI m_Cost;
        [SerializeField] private float selectInterval = 2f;

        public abstract void OnSelectionCompleted();

        public virtual void OnPointerDown()
        {
            StartCoroutine(PlaySelectAnimation(selectInterval));
        }

        public void OnPointerUp()
        {
            ResetFill();
        }

        public void OnPointerExit()
        {
            ResetFill();
        }

        private void ResetFill()
        {
            StopAllCoroutines();
            m_FillImage.fillAmount = 0f;

        }
        private IEnumerator PlaySelectAnimation(float interval)
        {
            var startTime = Time.time;
            while (Time.time - startTime<=interval)
            {
                var lerpValue = (Time.time - startTime) / interval;
                m_FillImage.fillAmount = lerpValue;
                yield return new WaitForEndOfFrame();
            }
            OnSelectionCompleted();
            m_FillImage.fillAmount = 0f;
        }
    }
}
