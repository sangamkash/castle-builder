﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CastleBuilder
{
    [System.Serializable]
    public class ShopItemData
    {
        public int id;
        public string imagePath;
        public string name;
        public string description;
        public uint goldCost;
        public uint level;
    }
}
