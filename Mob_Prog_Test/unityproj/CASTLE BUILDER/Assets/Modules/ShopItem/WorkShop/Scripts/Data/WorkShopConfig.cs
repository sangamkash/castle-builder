﻿using System.Collections;
using System.Collections.Generic;
using CastleBuilder.WorkShop.Data;
using UnityEngine;
using Util;

namespace CastleBuilder.WorkShop
{
    #region  Data

    [System.Serializable]
    public struct WorkShopConfigData
    {
#if UNITY_EDITOR
        [SerializeField] private Sprite shopImg;
#endif
        public WorkShopData workShopData;

#if UNITY_EDITOR
        public void AssignResourcePath()
        {
            workShopData.imagePath = shopImg.GetPath().GetResourcePath();
        }
#endif
    }
    #endregion
    

    [CreateAssetMenu(fileName = "WorkShopData", menuName = "CastleBuilder/WorkShop", order = 1)]
    public class WorkShopConfig : ScriptableObject
    {
        public WorkShopConfigData[] workShopConfigDatas;
#if UNITY_EDITOR
        [ContextMenu("AssignPath")]
        public void UpdateResourcePath()
        {
            foreach (var item in workShopConfigDatas)
            {
                item.AssignResourcePath();
            }
        }
#endif
        
    }
}
