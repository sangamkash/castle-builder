﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using CastleBuilder.WorkShop.Data;
using Unity.Entities;
using UnityEngine;

namespace CastleBuilder.WorkShop
{
    public struct WorkShopComponent : IComponentData
    {
        public int Id;
        public bool placed;
    }
}