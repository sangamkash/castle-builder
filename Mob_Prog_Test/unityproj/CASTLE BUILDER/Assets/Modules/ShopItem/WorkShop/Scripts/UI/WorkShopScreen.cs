﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CastleBuilder.WorkShop.UI
{

    public class WorkShopScreen : MonoBehaviour
    {
        [Header("External Dependency")] 
        [SerializeField] private GameObject m_ShopItemPrefab;
        [SerializeField] private WorkShopConfig m_WorkShopConfig;
        [Header("Internal Dependency")] 
        [SerializeField] private RectTransform m_Container;
        [SerializeField] private GameObject m_ShopScreen;
        private void Start()
        {
            Init();
        }

        private void Init()
        {
            for (var i = 0; i < m_WorkShopConfig.workShopConfigDatas.Length; i++)
            {
                var data = m_WorkShopConfig.workShopConfigDatas[i];
                var item = Instantiate(m_ShopItemPrefab, m_Container).GetComponent<WorkShopItem>();
                item.Init(data,OnWorkShopSelected);
            }
        }

        private void OnWorkShopSelected(WorkShopConfigData data)
        {
            m_ShopScreen.SetActive(false);
            ShopItemSpawner.pInstance.SpawnWorkShopItem(data.workShopData.imagePath, data.workShopData.id);
        }
    }
}
