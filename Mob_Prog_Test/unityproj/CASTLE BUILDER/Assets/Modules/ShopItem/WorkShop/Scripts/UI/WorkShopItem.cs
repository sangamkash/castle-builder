﻿using System;
using System.Collections;
using System.Collections.Generic;
using CastleBuilder.WorkShop.Data;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Video;

namespace CastleBuilder.WorkShop
{
    public class WorkShopItem : ShopItem
    {
        private Action<WorkShopConfigData> mAction;
        private WorkShopConfigData mData;
        public void Init(WorkShopConfigData data,Action<WorkShopConfigData> action)
        {
            mAction = action;
            mData = data;
            m_Image.sprite = Resources.Load<Sprite>(data.workShopData.imagePath);
            m_Tile.text = data.workShopData.name;
            m_Cost.text = data.workShopData.goldCost.ToString();
        }

        public override void OnPointerDown()
        {
            if (PlayerDataManager.pInstance.Purchase(mData.workShopData.goldCost))
                base.OnPointerDown();
            else
                UIManager.pInstance.ShowWaring("Don't have enough balance");
        }

        public override void OnSelectionCompleted()
        {
            mAction?.Invoke(mData);
        }
    }
}
