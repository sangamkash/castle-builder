﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using CastleBuilder.Decoration;
using CastleBuilder.WorkShop;
using UnityEngine;
using Util;

namespace CastleBuilder.Map
{
    [System.Serializable]
    public class MapItem
    {
        public int id;
        public Vector3 position;
    }
    
    [System.Serializable]
    public class MapData
    {
        public List<MapItem> mapItems;
    }
    
    /// <summary>
    /// This Script contain and Map related data and 
    /// </summary>
    public class MapSystem : MonoBehaviour
    {
        public static MapSystem pInstance { get; private set; }
        public MapData pMapData { get; private set; }
        
        [Header("External dependency")] 
        [SerializeField] private DecorationConfig m_DecorationConfig;
        [SerializeField] private WorkShopConfig m_WorkShopConfig;

        public ShopItemSpawner mShopItemSpawner=>ShopItemSpawner.pInstance;
        public Dictionary<int,string> mImagePathDic;
        private HashSet<Vector2> mOccupiedTile;

#if UNITY_EDITOR
        private string path => Application.dataPath + "/MapData.xml";
#else
        private string path => Application.persistentDataPath + "/MapData.xml";
#endif
        private void Awake()
        {
            pInstance = this;
            LoadData();
        }

        private void Start()
        {
            PopulateMap();
        }

        /// <summary>
        /// This function is called at the start ot the game to load last save data
        /// </summary>
        private void LoadData()
        {
            var fileExit = false;
            var json = path.ReadFile(out fileExit);
            if (string.IsNullOrEmpty(json))
            {
                pMapData = new MapData()
                {
                    mapItems = new List<MapItem>()
                };
            }
            else
            {
                try
                {
                    pMapData=JsonUtility.FromJson<MapData>(json);
                }
                catch (Exception ex)
                {
                    Debug.LogError($"fail to parse data as modified or corrupted");
                    pMapData = new MapData()
                    {
                        mapItems = new List<MapItem>()
                    };
                    path.DeleteFile();
                }
               
            }

            mImagePathDic = new Dictionary<int, string>();
            foreach (var item in m_DecorationConfig.decorationConfigDatas)
            {
                mImagePathDic.Add(item.decorationData.id, item.decorationData.imagePath);
            }
            foreach (var item in m_WorkShopConfig.workShopConfigDatas)
            {
                mImagePathDic.Add(item.workShopData.id, item.workShopData.imagePath);
            }
            mOccupiedTile= new HashSet<Vector2>();
            foreach (var mapItem in pMapData.mapItems)
            {
                mOccupiedTile.Add((Vector2) mapItem.position);
            }
        }

        /// <summary>
        /// This function is called at the start ot populate map from last saved data
        /// </summary>
        private void PopulateMap()
        {
            foreach (var mapItem in pMapData.mapItems)
            {
                mShopItemSpawner.SpawnWorkShopItem(mImagePathDic[mapItem.id],mapItem.id, mapItem.position);
            }
           
        }
        
        /// <summary>
        /// This function is called when ever any new item is placed in the map
        /// </summary>
        /// <param name="id"></param>
        /// <param name="position"></param>
        /// <param name="remove"></param>
        public void UpdateMapData(int id, Vector3 position,bool remove=false)
        {
            if (remove)
            {
                MapItem temp=null;
                foreach (var item in pMapData.mapItems)
                {
                    if (item.position.Equals(position) && item.id == id)
                        temp = item;
                }
                if (temp != null)
                    pMapData.mapItems.Remove(temp);
                return;
            }
            pMapData.mapItems.Add(new MapItem()
            {
                id=id,
                position = position
            });
            mOccupiedTile.Add((Vector2) position);
            path.WriteFile(JsonUtility.ToJson(pMapData,true));
        }

        /// <summary>
        /// This function tell about if the position is occupied by any item on Map
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public bool IsTileOccupied(Vector2 position)
        {
            return mOccupiedTile.Contains(position);
        }

        /// <summary>
        /// This function is call to update if the given position is occupied 
        /// </summary>
        /// <param name="position"></param>
        public void MakeTileAsUnOccupied(Vector2 position)
        {
            if (mOccupiedTile.Contains(position))
                mOccupiedTile.Remove(position);
        }
    }
}
