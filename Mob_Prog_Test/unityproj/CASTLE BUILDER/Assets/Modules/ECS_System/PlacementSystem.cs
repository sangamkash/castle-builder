﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using CastleBuilder.Grid;
using CastleBuilder.IsometricMaths;
using CastleBuilder.Map;
using CastleBuilder.WorkShop;
using CastleBuilder.WorkShop.Data;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.EventSystems;
using  UnityEngine.Rendering;
using NotImplementedException = System.NotImplementedException;

namespace CastleBuilder.ECS.Placement
{
    /// <summary>
    /// This Script take care of placing or replacing any item on Map
    /// </summary>
    public class PlacementSystem : ComponentSystem
    {
        public static bool pIsItemUnplaced { get; private set; }
        private float pGridsize => GridSystem.pInstance.pGridData.size;
        private UIManager pUIManger=>UIManager.pInstance;
        private MapSystem pMapSystem=>MapSystem.pInstance;
        private Dictionary<int, Material> mTransparentMatDic;
        private Dictionary<int, Material> mOriginalatDic;
        private int touchId;
        private Camera mCamera;
        private bool touchStarted;
        public Camera pCamera
        {
            get
            {
                if(mCamera == null)
                    mCamera=Camera.main;
                return mCamera;
            }
        }

        protected override void OnUpdate()
        {
            var entityManager = World.EntityManager;
            var allEntities =entityManager.GetAllEntities();
#if UNITY_EDITOR
            var touchPos = Input.mousePosition;
            var worldPoint = ((Vector2) pCamera.ScreenToWorldPoint(touchPos));
            if (Input.GetMouseButtonDown(0))
            {
                touchStarted = true;
                if (!EventSystem.current.IsPointerOverUIObject())
                    CheckForTouchOnItem(worldPoint, allEntities, entityManager);
            }

            if (Input.GetMouseButtonUp(0))
            {
                touchStarted = false;
                if (!EventSystem.current.IsPointerOverUIObject())
                    PlaceItem(worldPoint, allEntities, entityManager);
            }

            if (pIsItemUnplaced && EventSystem.current.IsPointerOverUIObject())
            {
                //Throw warning
            }
            else if(touchStarted)
            {
                Processtouch(allEntities, entityManager, worldPoint);
            }
                
#else
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if(touchId == -1)
                    touch = Input.GetTouch(0);
                {
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        var temp=Input.GetTouch(i);
                        if (temp.fingerId == touchId)
                            touch = temp;
                    } 
                }
                
                if (touch.phase == TouchPhase.Began && touchId == -1)
                {
                    touchStarted = true;
                    touchId = touch.fingerId;
                    var touchPos = touch.position;
                    var worldPoint = ((Vector2) pCamera.ScreenToWorldPoint(touchPos));
                    if (!EventSystem.current.IsFingureOverUIObject(touch))
                        CheckForTouchOnItem(worldPoint, allEntities, entityManager);
                }

                if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {
                    touchStarted = false;
                    touchId = -1;
                    var touchPos = touch.position;
                    var worldPoint = ((Vector2) pCamera.ScreenToWorldPoint(touchPos));
                    if (!EventSystem.current.IsFingureOverUIObject(touch))
                        PlaceItem(worldPoint, allEntities, entityManager);
                }

                if (pIsItemUnplaced && EventSystem.current.IsFingureOverUIObject(touch))
                {
                    //Throw warning
                }
                else if (touchStarted)
                {
                    var touchPos = touch.position;
                    var worldPoint = ((Vector2) pCamera.ScreenToWorldPoint(touchPos));
                    Processtouch(allEntities, entityManager, worldPoint);
                }
            }
#endif
            allEntities.Dispose();
        }

        private void Processtouch(NativeArray<Entity> allEntities, EntityManager entityManager, Vector2 worldPoint)
        {
            foreach (var entity in allEntities)
            {
                if (!entityManager.HasComponent<WorkShopComponent>(entity))
                    continue;
                var workShopComponent = entityManager.GetComponentData<WorkShopComponent>(entity);
                var translation = entityManager.GetComponentData<Translation>(entity);
                var renderMesh = entityManager.GetSharedComponentData<RenderMesh>(entity);
                if (UpdateShopItem(ref workShopComponent, ref translation, ref renderMesh, worldPoint))
                {
                    entityManager.SetComponentData(entity, workShopComponent);
                    entityManager.SetComponentData(entity, translation);
                    entityManager.SetSharedComponentData(entity, renderMesh);
                }
            }
        }

        private void CheckForTouchOnItem(Vector2 worldPoint, NativeArray<Entity> allEntities, EntityManager entityManager)
        {
           Debug.Log("On touch button begain 1");
            var gridPoint = worldPoint.ToNearestIsometricPoint(pGridsize);
            
            foreach (var entity in allEntities)
            {
                if (!entityManager.HasComponent<WorkShopComponent>(entity))
                    continue;
                var workShopComponent = entityManager.GetComponentData<WorkShopComponent>(entity);
                var translation = entityManager.GetComponentData<Translation>(entity);
                if (!workShopComponent.placed)
                {
                    
                    Debug.Log("On touch button begain 2");

                    var renderMesh = entityManager.GetSharedComponentData<RenderMesh>(entity);
                    GetMaterial(workShopComponent.Id, renderMesh.material);
                    return;
                }
            }
            foreach (var entity in allEntities)
            {
                if (!entityManager.HasComponent<WorkShopComponent>(entity))
                    continue;
                var translation = entityManager.GetComponentData<Translation>(entity);
                if (Mathf.Approximately(translation.Value.x, gridPoint.x) &&
                    Mathf.Approximately(translation.Value.y, gridPoint.y))
                {
                    var workShopComponent = entityManager.GetComponentData<WorkShopComponent>(entity);
                    var renderMesh = entityManager.GetSharedComponentData<RenderMesh>(entity);
                    pMapSystem.UpdateMapData(workShopComponent.Id, new Vector3(translation.Value.x, translation.Value.y, translation.Value.z), true);
                    pMapSystem.MakeTileAsUnOccupied(gridPoint);
                    workShopComponent.placed = false;
                    entityManager.SetComponentData(entity, workShopComponent);
                    GetMaterial(workShopComponent.Id, renderMesh.material);
                    Debug.Log("On touch button begain 3");

                    break;
                }
            }
        }

        private void PlaceItem(Vector2 worldPoint, NativeArray<Entity> allEntities, EntityManager entityManager)
        {
            var position = worldPoint.ToNearestIsometricPoint(pGridsize);
            var isOccupied= pMapSystem.IsTileOccupied(position);
            foreach (var entity in allEntities)
            {
                if (!entityManager.HasComponent<WorkShopComponent>(entity))
                    continue;
                var workShopComponent = entityManager.GetComponentData<WorkShopComponent>(entity);
                var translation = entityManager.GetComponentData<Translation>(entity);
                var renderMesh = entityManager.GetSharedComponentData<RenderMesh>(entity);
                
                if(!workShopComponent.placed && !isOccupied)
                {
                    workShopComponent.placed = true;
                    renderMesh.material = mOriginalatDic[workShopComponent.Id];
                    entityManager.SetComponentData(entity, workShopComponent);
                    entityManager.SetComponentData(entity, new Translation()
                    {
                        Value=new float3(translation.Value.x,translation.Value.y,0f)
                    });
                    entityManager.SetSharedComponentData(entity, renderMesh);
                    pMapSystem.UpdateMapData(workShopComponent.Id, new Vector3(translation.Value.x, translation.Value.y, 0f));
                    pIsItemUnplaced = false;
                }
                else if (!workShopComponent.placed && isOccupied)
                {
                    pIsItemUnplaced = true;
                    pUIManger.ShowWaring("Improper position ");
                    Debug.Log($"Display warning");
                }
            }
        }
        private bool UpdateShopItem(ref WorkShopComponent workShopComponent,ref Translation translation,ref RenderMesh renderMesh,Vector2 worldPoint)
        {
            var applyChanges = false;
            var position = worldPoint.ToNearestIsometricPoint(pGridsize);
            var isOccupied= pMapSystem.IsTileOccupied(position);
            if (!workShopComponent.placed)
            {
                translation.Value.x = position.x;
                translation.Value.y = position.y;
                translation.Value.z = -1f;
                renderMesh.material = GetMaterial(workShopComponent.Id, renderMesh.material);
                renderMesh.material.color = isOccupied ? new Color(1f, 0f, 0f, 0.9f) : new Color(1f, 1f, 1f, 0.5f);
                applyChanges = true;
            }
            return applyChanges;
        }

        private Material GetMaterial(int id,Material material)
        {
            if(mTransparentMatDic== null)
                mTransparentMatDic= new Dictionary<int, Material>();
            if (mOriginalatDic == null)
                mOriginalatDic = new Dictionary<int, Material>();
            if (mTransparentMatDic.ContainsKey(id))
                return mTransparentMatDic[id];
            
            var texture = material.mainTexture;
            if (!mOriginalatDic.ContainsKey(id))
            {
                mOriginalatDic.Add(id, material);
            }
            var mat = new Material(material)
            {
                mainTexture = texture,
                color=new Color(1f, 1f, 1f, 0.5f)
            };
            mTransparentMatDic.Add(id,mat);
            return mat;
        }
    }
}