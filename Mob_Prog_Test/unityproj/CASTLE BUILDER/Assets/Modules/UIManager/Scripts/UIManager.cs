﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace CastleBuilder
{
    public class UIManager : MonoBehaviour
    {
       public static UIManager pInstance { get; private set; }

       [Header("Screen ref")] 
       [SerializeField] private WarningPopup m_WarningPopup;

       [Header("InterDependence")] 
       [SerializeField] private TextMeshProUGUI m_PlayerName;
       [SerializeField] private TextMeshProUGUI m_Level;
       [SerializeField] private TextMeshProUGUI m_Coin;
       [SerializeField] private Image m_LevelProgressBar;
       [SerializeField] private Image m_HappinessImageBar;
       private void Awake()
       {
           pInstance = this;
       }

       private void Start()
       {
           OnPlayerDataChange(PlayerDataManager.pInstance.GetPlayerData());
       }

       private void OnEnable()
       {
           PlayerDataManager.pInstance.OnPlayerDataChange += OnPlayerDataChange;
       }
       private void OnDisable()
       {
           PlayerDataManager.pInstance.OnPlayerDataChange -= OnPlayerDataChange;
       }
       private void OnPlayerDataChange(PlayerData data)
       {
           m_PlayerName.text = data.playerName;
           m_Level.text = data.level.ToString();
           m_Coin.text = data.coin.ToString();
           m_LevelProgressBar.fillAmount = Mathf.Clamp01(data.level / 10000f);
           m_HappinessImageBar.fillAmount=Mathf.Clamp01(data.happiness / 10000f);
       }
       public void ShowWaring(string warningMsg)
       {
           m_WarningPopup.ShowWarning(warningMsg);
       }
    }
}
