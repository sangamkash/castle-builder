﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WarningPopup : BaseScreen
{
    [SerializeField] private TextMeshProUGUI m_WarningTxt;
    
    public void ShowWarning(string message)
    {
        Activate();
        m_WarningTxt.text = message;
        Invoke("Deactivate",0.5f);
    }
}
